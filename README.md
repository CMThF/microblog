microblog
=========

A (micro)blog and profile website for personal use, based on Flask, to be deployed on heroku for free - yey!

Features:
=========

* Blog entries
* "Milestone" entries
* (limited) Full Text Search

Requirements:
=============
* heroku account and heroku toolbelt
* Flask
* Flask-SQLAlchemy
* Jinja2
* SQLAlchemy
* Werkzeug
* gunicorn
* passlib
* psycopg2
* virtualenv
