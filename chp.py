# -*- coding: utf-8 -*-
#!flask/bin/python

'''                      copyright by Clemens Meinhart                      '''

from __future__ import with_statement
from werkzeug import secure_filename
from passlib.apps import custom_app_context as pwd_context
from flask import Flask, request, session, redirect, url_for, abort, \
     render_template, flash, _app_ctx_stack
from flask.ext.sqlalchemy import SQLAlchemy
from datetime import datetime
import re

import config

# create our little application :)
app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)

@app.teardown_appcontext
def close_db_connection(exception):
    """Closes the database again at the end of the request."""
    top = _app_ctx_stack.top
    if hasattr(top, 'sqlite_db'):
        top.sqlite_db.close()


@app.route('/index')
def index():
    return redirect(url_for('show_entries'))


'''                ---------- Microblog Entries ----------                '''
@app.route('/')
def show_entries(error=None):
    try:
        entries = Entry.query.order_by('last_change desc').all()
        for e in entries:
            if len(e.entrytext) > config.PREVIEW:
                e.entrytext = re.sub('<[^>]*>', '', e.entrytext)[:config.PREVIEW] + '...'
        if not error:
            return render_template('show_entries.html', entries=entries)
        return render_template('show_entries.html', entries=entries, error=error)
    except Exception, e:
        error = 'An unexpected error occured.'
        if config.DEBUG:
            error += ' in show_entries: '+str(e)
        logout()
        flash(error, 'error')
        abort(404)        
        #return redirect(url_for('login'))

        
@app.route('/create_entry')
def create_entry():
    if not session.get('logged_in'):
        abort(401)
    return render_template('create_entry.html')


@app.route('/e/<entrytitle>/delete')
def delete_entry(entrytitle):
    try:
        if not session.get('logged_in'):
            abort(401)

        entry = Entry.query.filter_by(title=entrytitle).first_or_404()        
        
        db.session.delete(entry)
        db.session.commit()
        flash('Entry deleted.')
        return redirect(url_for('show_entries'))
    except Exception, e:
        if str(e) == "404: Not Found":
            abort(404)
        error = 'An unexpected error occured. Try again later.'
        if config.DEBUG:
            error += ' in delete_entry: '+str(e)
        flash(error, 'error')
        return redirect(url_for('show_entries'))

        
@app.route('/add_entry', methods=['GET', 'POST'])
def add_entry():
    try:
        if not session.get('logged_in'):
            abort(401)

        title = unicode(request.form['title'])
        entry = Entry(title, unicode(request.form['text']))
        db.session.add(entry)

        tags = unicode(request.form['tags']).split(',')
        for tag in tags:
            t = Tag.query.filter_by(title=tag.strip()).first()
            if not t:
                t = Tag(tag.strip())
            entry.tags.append(t)

        db.session.commit()
        flash('Successfully added new entry.')
        return redirect(url_for('show_entries'))
    except Exception, e:
        error = 'An unexpected error occured. Try again later.'
        if config.DEBUG:
            error += ' in add_entry: '+str(e)
        flash(error, 'error')
        return redirect(url_for('show_entries'))

        
@app.route('/e/<entrytitle>')
def show_entry(entrytitle):
    try:
        entry = Entry.query.filter_by(title=entrytitle).first_or_404()
        return render_template('show_entry.html', entry=entry)
    except Exception, e:
        if str(e) == "404: Not Found":
            abort(404)
        error = 'An unexpected error occured. Try again later.'
        if config.DEBUG:
            error += ' in show_entry: '+str(e)
        flash(error, 'error')
        return redirect(url_for('show_entries'))


@app.route('/e/<entrytitle>/edit')
def editmode(entrytitle):
    try:
        if not session.get('logged_in'):
            abort(401)
        entry = Entry.query.filter_by(title=entrytitle).first_or_404()
        return render_template('edit_entry.html', entry=entry)
    except Exception, e:
        if str(e) == "404: Not Found":
            abort(404)
        error = 'An unexpected error occured. Try again later.'
        if config.DEBUG:
            error += ' in editmode: '+str(e)
        flash(error, 'error')
        return redirect(url_for('show_entries'))


@app.route('/e/<entrytitle>/changed', methods=['GET', 'POST'])
def edit_entry(entrytitle):
    try:
        if not session.get('logged_in'):
            abort(401)
        
        title = unicode(request.form['title'])
        
        old_entry = Entry.query.filter_by(title=entrytitle).first()
        old_entry.title = title
        old_entry.entrytext = unicode(request.form['text'])
        
        tags = unicode(request.form['tags']).split(',')
        tags = [t.strip() for t in tags]
        for t in entry.tags:
            if t.title not in tags:
                entry.tags.remove(t)
        for tag in tags:
            t = Tag.query.filter_by(title=tag).first()
            if not t:
                t = Tag(tag)
            if t not in entry.tags:
                entry.tags.append(t)
        
        db.session.commit()
        flash('Changes saved.')
        return show_entry(title)
    except Exception, e:
        error = 'An unexpected error occured. Try again later.'
        if config.DEBUG:
            error += ' in edit_entry: '+str(e)
        flash(error, 'error')
        return redirect(url_for('show_entries'))


'''                ---------- Milestone Entries ----------                '''
@app.route('/create_milestone')
def create_milestone():
    if not session.get('logged_in'):
        abort(401)
    return render_template('create_milestone.html')


@app.route('/m/<milestonetitle>/up')
def milestone_up(milestonetitle):
    try:
        if not session.get('logged_in'):
            abort(401)
        ms = Milestone.query.filter_by(title=milestonetitle).first()
        ms.position -= 1
        db.session.commit()
        return show_milestones()
    except Exception, e:
        error = 'An unexpected error occured. Try again later.'
        if config.DEBUG:
            error += ' in milestone up: '+str(e)
        flash(error, 'error')
        return redirect(url_for('show_milestones'))

        
@app.route('/m/<milestonetitle>/down')
def milestone_down(milestonetitle):
    try:
        if not session.get('logged_in'):
            abort(401)
        ms = Milestone.query.filter_by(title=milestonetitle).first()
        ms.position += 1
        db.session.commit()
        return show_milestones()
    except Exception, e:
        error = 'An unexpected error occured. Try again later.'
        if config.DEBUG:
            error += ' in milestone up: '+str(e)
        flash(error, 'error')
        return redirect(url_for('show_milestones'))


@app.route('/milestones')
def show_milestones(error=None):
    try:
        milestones = Milestone.query.order_by('position asc').all()
        if not error:
            return render_template('show_milestones.html', milestones=milestones)
        return redirect(url_for('show_entries'))
    except Exception, e:
        error = 'An unexpected error occured.'
        if config.DEBUG:
            error += '<br> in milestones: '+str(e)
        logout()
        flash(error, 'error')
        abort(404)        


@app.route('/m/<milestonetitle>/delete')
def delete_milestone(milestonetitle):
    try:
        if not session.get('logged_in'):
            abort(401)

        milestone = Milestone.query.filter_by(title=milestonetitle).first_or_404()        
        
        db.session.delete(milestone)
        db.session.commit()
        flash('Milestone deleted.')
        return redirect(url_for('show_milestones'))
    except Exception, e:
        if str(e) == "404: Not Found":
            abort(404)
        error = 'An unexpected error occured. Try again later.'
        if config.DEBUG:
            error += ' in delete_milestone: '+str(e)
        flash(error, 'error')
        return redirect(url_for('show_entries'))

        
@app.route('/add_milestone', methods=['GET', 'POST'])
def add_milestone():
    try:
        if not session.get('logged_in'):
            abort(401)

        title = unicode(request.form['title'])
        milestone = Milestone(title, unicode(request.form['text']), unicode(request.form['timespan']))
        db.session.add(milestone)
        db.session.commit()
        flash('Successfully added new milestone.')
        return redirect(url_for('show_milestones'))
    except Exception, e:
        error = 'An unexpected error occured. Try again later.'
        if config.DEBUG:
            error += ' in add_milestone: '+str(e)
        flash(error, 'error')
        return redirect(url_for('show_entries'))
        

@app.route('/m/<milestonetitle>/edit')
def edit_milestone(milestonetitle):
    try:
        if not session.get('logged_in'):
            abort(401)
        milestone = Milestone.query.filter_by(title=milestonetitle).first_or_404()
        return render_template('edit_milestone.html', milestone=milestone)
    except Exception, e:
        if str(e) == "404: Not Found":
            abort(404)
        error = 'An unexpected error occured. Try again later.'
        if config.DEBUG:
            error += ' in edit_milestone: '+str(e)
        flash(error, 'error')
        return redirect(url_for('show_entries'))


@app.route('/m/<milestonetitle>/changed', methods=['GET', 'POST'])
def edited_milestone(milestonetitle):
    try:
        if not session.get('logged_in'):
            abort(401)
        
        title = unicode(request.form['title'])
        
        old_milestone = Milestone.query.filter_by(title=milestonetitle).first()
        old_milestone.title = title
        old_milestone.milestonetext = unicode(request.form['text'])
        old_milestone.timespan = unicode(request.form['timespan'])
        
        db.session.commit()
        
        flash('Changes saved.')
        return show_milestones()
    except Exception, e:
        if str(e) == "404: Not Found":
            abort(404)
        error = 'An unexpected error occured. Try again later.'
        if config.DEBUG:
            error += ' in edited_milestone: '+str(e)
        flash(error, 'error')
        return redirect(url_for('show_entries'))


'''                        ---------- Tags ----------                     '''
@app.route('/t/<tagtitle>')
def show_tag(tagtitle):
    try:
        tag = Tag.query.filter_by(title=tagtitle).first_or_404()
        return render_template('show_tag.html', tag=tag)
    except Exception, e:
        if str(e) == "404: Not Found":
            abort(404)
        error = 'An unexpected error occured. Try again later.'
        if config.DEBUG:
            error += ' in show_tag: '+str(e)
        flash(error, 'error')
        return redirect(url_for('show_entries'))

        
        
'''                   ---------- About Route ----------                   '''
@app.route('/about')
def about():
    return render_template('about.html')
    
    
'''                 ---------- Error Handlers ----------                  '''
@app.errorhandler(401)
def unauthorized(e):
    return render_template('401.html'), 401


@app.errorhandler(403)
def forbidden(e):
    return render_template('403.html'), 403


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404
        

'''                 ---------- Search Handling ----------                 '''
@app.route('/searchsite')
def searchsite():
    return render_template('searchsite.html')

@app.route('/search', methods=['GET', 'POST'])
def search(): 
    ''' Given the user's input, returns a list of 3-tuples: blog post object, 
    a list of fragments containing search terms with <span class="highlight">
    </span> around the search terms and the blog title also containing 
    <span class="highlight"></span> around each search term. '''
    searchterms = unicode(request.form['searchbar'])
    #searchterms = searchterms.split(' ')
    # search_vector is a ts_vector column. To search for terms, you use the 
    # @@ operator. plainto_tsquery turns a string into a query that can be 
    # used with @@. So this adds a where clause like "WHERE search_vector 
    # @@ plaint_tsquery(<search string>)"
    #q = session.query(Entry).filter('entry.search_vector '\
    #                             '@@ plainto_tsquery(:terms)')
    #q = Entry.query.filter('entry.search_vector '\
    #                 '@@ plainto_tsquery(:terms)')
    q = Entry.query.filter('entry.search_vector '\
                     '@@ plainto_tsquery(\'english\',:terms)')

    # This binds the :terms placeholder to the searchterms string. User input 
    # should always be put into queries this way to prevent SQL injection.
    q = q.params(terms=searchterms)

    # This adds an extra column that is the blog contents made into a "headline"
    # (bunch of fragments) using the postgresql function ts_headline. The 4th 
    # argument is a string giving options to the function. StartSel and StopSel 
    # give the strings the search terms will be highlighted with. MaxFragments 
    # gives the maximum number of fragments returned and FragmentDelimiter give a 
    # string that will separate the fragments. We use this to split the fragments 
    # into a list later.
    '''q = q.add_column(func.ts_headline('pg_catalog.english', 
                   Entry.infrastructure, 
                   func.plainto_tsquery(searchterms),
                   'MaxFragments=5,FragmentDelimiter=|||,'\
                   'StartSel="<span class=""highlight"">", '\
                   'StopSel = "</span>", ',
                   type_= Unicode))

    # This is very similar to above, only instead of using fragments, we pass the 
    # option. HighlightAll=TRUE which means the whole field (Blog.title) will be 
    # returned with highlighting instead of a section of the title.
    q = q.add_column(func.ts_headline('pg_catalog.english', 
                   Entry.title, 
                   func.plainto_tsquery(searchterms),
                   'HighlightAll=TRUE, '\
                   'StartSel="<span class=""highlight"">", '\
                   'StopSel = "</span>"',
                   type_= Unicode))'''

    # This calls ts_rank_cd with the search_vector and the query and gives a ranking 
    # to each row. We order by this descending. Again, the :terms placeholder is used 
    # to insert user input.
    #q = q.order_by('ts_rank_cd(entry.search_vector, '\
    #             'plainto_tsquery(:terms)) DESC')
    q = q.order_by('ts_rank_cd(entry.search_vector, '\
                 'plainto_tsquery(\'english\',:terms)) DESC')

    # Because of the two add_column calls above, the query will return a 3-tuple
    # consisting of the actual entry objects, the fragments for the contents and
    # the highlighted headline. In order to make the fragments a list, we split them
    # on '|||' - the FragmentDelimiter.
    #entries, fragments, titles = [(entry, fragments.split('|||'), title) for entry, fragments, title in q]
    entries = q
    
    return render_template('search.html', entries=entries)


'''                  ---------- User Handling ----------                  '''
@app.route('/login', methods=['GET', 'POST'])
def login():
    try:
        error = None
        if request.method == 'POST':

            username = unicode(request.form['username'])
            user = User.query.filter_by(username=username).first()
                
            if not user:
                error = 'Invalid username or password.'
            elif pwd_context.verify(request.form['password'], user.pw_hash):
                session['logged_in'] = True
                session['user'] = username
                flash('You are now logged in.')
                session.permanent = False
                return redirect(url_for('show_entries'))
            else:
                error = 'Invalid password'
        return render_template('login.html', error=error)
    except Exception, e:
        error = 'An unexpected error occured. Try again later.'
        if config.DEBUG:
            error += ' in login: '+str(e)
        return render_template('login.html', error=error)


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    session.pop('user', None)
    flash('You have been logged out.')
    return redirect(url_for('show_entries'))
    
    
# Helper method to change the user's password
def change_pw(name, oldpw, pw):
    try:
        user = User.query.filter_by(username=name).first()
        error = 'done'
        if not user:
            error = 'Invalid username or password.'
        elif pwd_context.verify(oldpw, user.pw_hash):
            user.pw_hash = pwd_context.encrypt(pw)
            db.session.commit()
        else:
            error = 'Invalid username or password.'
        print error
    except Exception, e:
        error = 'An unexpected error occured. Try again later.'
        print error

        
'''            ---------- Databse schema and models ----------            '''
class User(db.Model):
    user_id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True)
    pw_hash = db.Column(db.String(400))
    email = db.Column(db.String(120), unique=True)
    

    def __init__(self, username, pw_hash, email):
        self.username = username
        self.pw_hash = pw_hash
        self.email = email


    def __repr__(self):
        return '<User %r>' % self.username


class Milestone(db.Model):
    milestone_id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Unicode)
    milestonetext = db.Column(db.Unicode)
    timespan = db.Column(db.Unicode)
    position = db.Column(db.Integer)
    
    
    def __init__(self, title, milestonetext, timespan):
        self.title = title
        self.milestonetext = milestonetext
        self.timespan = timespan
        self.position = 0
    
    def __repr__(self):
        return '<Milestone %r>' % self.title
    

tags = db.Table('tags',
    db.Column('tag_id', db.Integer, db.ForeignKey('tag.id')),
    db.Column('entry_id', db.Integer, db.ForeignKey('entry.entry_id'))
)


class Tag(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Unicode)

    def __init__(self, title):
        self.title = title
        

    def __repr__(self):
        return '<Tag %r>' % self.title

    
class Entry(db.Model):
    entry_id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Unicode, unique=True)
    last_change = db.Column(db.DateTime)
    entrytext = db.Column(db.Unicode)
    tags = db.relationship('Tag', secondary=tags, backref=db.backref('entries', lazy='dynamic'))
    
    
    def __init__(self, title, entrytext, last_change=None):
        self.title = title
        if last_change is None:
            last_change = datetime.utcnow()
        self.last_change = last_change
        self.entrytext = entrytext


    def __repr__(self):
        return '<Entry %r>' % self.title
        


        
'''             ---------- Full Text Search Setup ----------             '''
def setup_search(event, schema_item, bind):
    ''' Automatically run when SQLAlchemy creates the tables e.g. by running 
    Base.metadata.create_all(bind=db) '''

    # We don't want sqlalchemy to know about this column so we add it externally.
    bind.execute("alter table entry add column search_vector tsvector")
    
    # This indexes the tsvector column
    bind.execute("""create index entry_search_index on entry 
                     using gin(search_vector)""")
    
    # This sets up the trigger that keeps the tsvector column up to date.
    bind.execute("""create trigger entry_search_update before update or 
                    insert on entry
                    for each row execute procedure
                    tsvector_update_trigger('search_vector', 
                                            'pg_catalog.simple', 
                                            'entrytext', 'title')""")        


'''                ---------- Setup Database ----------                   '''
def setup(name, pw, email):
    # add default admin
    db.drop_all()
    # We want to call setup_search after the blog_entries has been created.
    Entry.__table__.append_ddl_listener('after-create', setup_search)
    db.create_all()
    admin = User(name, pwd_context.encrypt(pw), email)
    db.session.add(admin)
    db.session.commit()
    #app.run(debug=config.DEBUG)

def set_debug(d):
    config.DEBUG = d
    

if __name__ == '__main__':
    setup()

