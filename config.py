import os

# configuration
DEBUG = False
PREVIEW = 50
SECRET_KEY = 'get your own'

#if os.environ.get('DATABASE_URL') is None:
#    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:default@localhost/chp'
#else:
if os.environ.get('DATABASE_URL'):
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
