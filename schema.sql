drop table if exists users;
create table users (
  user_id integer primary key autoincrement,
  username text not null unique,
  pw_hash text not null,
  email text not null unique
);

drop table if exists entries;
create table entries (
  entry_id integer primary key autoincrement,
  title text not null,
  last_change datetime default current_timestamp, 
  entrytext text
);


drop table if exists milestones;
create table milestones (
  milestone_id integer primary key autoincrement,
  title text not null,
  milestonetext text,
  timespan text
);
